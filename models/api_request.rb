class ApiRequest
  include DataMapper::Resource

  property :id, Serial, :key => true
  property :hashkey, String
  property :response, Text
  property :created_at, DateTime 
  
  DataMapper.finalize #this is needed for heroku. for some strange reason datamapper fails without this one here 
  
  class << self
    def get_from_cache hash_key
      time = Time.now
      last(hashkey: hash_key, created_at: ((time - API.get_config(:cache_invalidation_period))..time))
    end
    
    def put_to_cache hash_key, data
      create(hashkey: hash_key, created_at: Time.now, response: data)
    end
    
    #Just in case there will a strong need to flush hash
    def clear_cache
      all.detroy
    end
    
  end
end