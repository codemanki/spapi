require 'digest/sha1'
require 'net/http'
require 'json'
require './app'

directories = %w(lib models)

directories.each do |directory|
  Dir["#{File.dirname(__FILE__)}/#{directory}/*.rb"].each do |file|
    require file
  end
end