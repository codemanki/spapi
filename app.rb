require 'sinatra'
require 'data_mapper'

class SpApp < Sinatra::Base

  configure do
    set :public_folder, Proc.new { File.join(root, "static") }
    
    DataMapper.setup(:default, ENV['DATABASE_URL'] || "sqlite3://#{Dir.pwd}/demo.db")
    DataMapper.finalize
    DataMapper.auto_migrate!
  end

  get '/' do
    erb :index
  end

  post '/askApi' do 
    api = API::ApiFetcher.new(params[:params])
    response = api.perform_request
  
    content_type :json 
    response
  end
  
end
