module API
  CONFIG = {
    api_params: {
      appid: "157",
      device_id: "2b6f0cc904d137be2e1730235f5664094b831186",
      locale: "de",
      ip: "109.235.143.113",
      offer_types: 112,
    },
    api_key: "b07a12df7d52e6c118e5d47d3f9e60135b109a1f",
    api_uri: "http://api.sponsorpay.com/feed/v1/offers.json",
    cache_invalidation_period: 15 * 60,
    header_signature_header: "X-Sponsorpay-Response-Signature",
    error_response: '{"code":"ERROR"}'
  }

  class << self
      def get_config(key)
        CONFIG[key]
      end
  end
end