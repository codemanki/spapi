module API
  class ApiRequestParams
    class << self
      
      #Merges params from user with params from config file and sorts
      def construct_params params
        sort_hash_by_key(API.get_config(:api_params).merge(params))
      end
    
      #Takes hash from url, which is generated from given params
      def generate_hashkey params
        url = "#{construct_url(params)}&#{API.get_config(:api_key)}"
        Digest::SHA1.hexdigest(url).downcase #hashkey should always be downcased
      end
      
      #Joins hashes key and value into pair with &
      def construct_url params
        params.map{|key, value| "#{key}=#{value}"}.join('&')
      end
      
      #Forms final url for api call
      def get_request_url input_params
        #First, insert timestamp to  params and clone it
        params = sort_hash_by_key(input_params.merge({timestamp: Time.now.to_i})) 
        #Step two, get hash
        hashkey = generate_hashkey(params)
        #Step three, append hash
        url = "#{construct_url(params)}&hashkey=#{hashkey}"
      end
      
      def sort_hash_by_key hash
        Hash[hash.sort_by{|hsh| hsh[0].to_s}]
      end
      
    end
  end
end