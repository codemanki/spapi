module API
  class ApiFetcher
    
    def initialize params = {}
      @input_params = params
    end
    
    def perform_request
      #first, validate if request is valid
      return API.get_config(:error_response) unless params_valid? @input_params
      
      #Then merge params
      @user_params = ApiRequestParams.construct_params(@input_params) 
      
      #Ask cache about params. We do not insert timestamp, because it will ruin the hashkey.
      hash_key = ApiRequestParams.generate_hashkey @user_params
      
      cached = ApiRequest.get_from_cache(hash_key)
      
      if(!cached)
        #If it is not in cache, then generate url with timestamp and other data, send request and cache it
        request = get(ApiRequestParams.get_request_url(@user_params))
        body = request.body
        #Check response
        if response_valid? request
          cache_response(hash_key, body)
        else
          #just respond with error
          body = API.get_config(:error_response)
        end
        return body
      else
        return cached.response
      end
    end
    
    private
    def cache_response hash_key, response
      json = JSON.parse(response)
      #response should be cached only if it was successfull
      ApiRequest.put_to_cache(hash_key, response) if json["code"] && json["code"].downcase == "ok"
    end
    
    def get url
      url = URI.parse("#{API.get_config(:api_uri)}?#{url}")
      Net::HTTP.get_response(url)
    end
    
    def response_valid? response 
      Digest::SHA1.hexdigest((response.body || "") + API.get_config(:api_key)) == response[API.get_config(:header_signature_header)]
    end
    
    def params_valid? params
      #Params should include 3 keys uid, pub0, page and not be nil :)
      params && params.has_key?("uid") && params.has_key?("pub0") && params.has_key?("page")
    end
  end
end