require 'spec_helper'

describe "General tests for app routes" do
  
  def app
    @app ||= SpApp.new
  end
  
  before(:all) do
    @params = {uid:123, page: 1, pub0: 0}
  end

  describe "GET '/'" do
    it "should be successful" do
      get '/'
      last_response.should be_ok
    end
    
    it "should return some html" do
      get '/'
      last_response.body.should include("<title>SponsorPay</title>")
    end
  end
  
  describe "when asking api" do
    it "should be successful" do
      get '/askApi'
      last_response.should_not be_ok
    end
    
    it "should return error since no params provided" do
      post '/askApi'
      last_response.body.should include("ERROR")
    end
    
    it "should return error since one of params is not provided" do
      post '/askApi', @params.select{|x| x != :page} # passing hash without one key, fast hack
      last_response.body.should include("ERROR")
    end
  end
end