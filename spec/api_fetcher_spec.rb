require 'spec_helper'


describe API::ApiFetcher do
  
  before(:all) do
    @params = {"uid" => 123, "page" => 1, "pub0" => 0}
    @happy_path_response = '{"code":"OK"}'
    @happy_path_header = API::get_config(:header_signature_header)
    @happy_path_code = sha1(@happy_path_response + API::get_config(:api_key))
    @reg = /.*sponsorpay.*/
    @error_response = API.get_config(:error_response)
  end
  
  describe "with params" do
    it "should return error because params are empty" do 
      @fetcher = API::ApiFetcher.new()
      @fetcher.perform_request().should eql @error_response
    end
    
    it "should return error when one of keys is not in params" do
      @params.each do |key, value|
        @fetcher = API::ApiFetcher.new(@params.select{|x| x != key})
        @fetcher.perform_request().should eql @error_response
      end
    end
    
    it "should return something, since this is a happy path!" do
      stub = stub_request(:get, @reg).to_return(:body => @happy_path_response, :status => 200, :headers => { @happy_path_header => @happy_path_code })
      @fetcher = API::ApiFetcher.new(@params)
      @fetcher.perform_request().should eql @happy_path_response
      stub.should have_been_requested
    end
  end
  
  it "should not call http request for second time and get value from cache" do 
    stub = stub_request(:get, @reg).to_return(:body => @happy_path_response, :status => 200, :headers => { @happy_path_header => @happy_path_code }).times(1)
    @fetcher = API::ApiFetcher.new(@params)
    #For three calls with same info it should make only 1 http call
    @fetcher.perform_request().should eql @happy_path_response
    @fetcher.perform_request().should eql @happy_path_response
    @fetcher.perform_request().should eql @happy_path_response
  end
  
  
  it "should return error, because response is corrupted" do
    stub = stub_request(:get, @reg).to_return(:body => @happy_path_response + "HACK HACK", :status => 200, :headers => { @happy_path_header => @happy_path_code })
    @fetcher = API::ApiFetcher.new(@params)
    @fetcher.perform_request().should eql @error_response
  end
  
  it "should return error, because these is no header with hash" do
    stub = stub_request(:get, @reg).to_return(:body => @happy_path_response, :status => 200)
    @fetcher = API::ApiFetcher.new(@params)
    @fetcher.perform_request().should eql @error_response
  end
  
  it "should make http calls for same data if previous response wasn't successfull" do
    response = '{"code":"ERROR"}'
    code = sha1(response + API::get_config(:api_key))
    stub_request(:get, @reg).to_return(:body => response, :status => 200, :headers => { @happy_path_header => code }).times(2)
    @fetcher = API::ApiFetcher.new(@params)
    @fetcher.perform_request()
    @fetcher.perform_request()
  end
  
  it "should handle errors from http and return regular error" do 
    stub_request(:get, @reg).to_return(:body => "response", :status => 500)
    @fetcher = API::ApiFetcher.new(@params)
    @fetcher.perform_request().should eql @error_response
  end
end