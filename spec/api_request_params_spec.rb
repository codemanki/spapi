require 'spec_helper'

describe API::ApiRequestParams do

  before(:all) do
    @params = {param1: 1, param2: 2}
  end
  
  it "should return merged params on construct_params" do
    output_params = API::ApiRequestParams.construct_params(@params)
    
    output_params.should have_key :param1
    output_params.should have_key :param2
    output_params.should have_key API::get_config(:api_params).keys[0] #and some key from api_params
  end
  
  it "should return same hash for same params" do 
    hash_1 = API::ApiRequestParams.generate_hashkey(@params)
    hash_2 = API::ApiRequestParams.generate_hashkey(@params)
    
    hash_1.should eql hash_2
  end
  
  it "should return different hashes for different params" do
    hash_1 = API::ApiRequestParams.generate_hashkey(@params)
    hash_2 = API::ApiRequestParams.generate_hashkey(@params.merge({param3: 3}))
    
    hash_1.should_not  eql hash_2 
  end
  
  it "should return url with all passed params" do 
    url = API::ApiRequestParams.construct_url(@params)
    
    @params.each do |key, value|
      url.should include("#{key}=#{value}")
    end
  end
  
  it "should return url with timestamp" do 
    url = API::ApiRequestParams.get_request_url(@params)
    url.should include("timestamp")
  end
  
  it "should return url without api key" do 
    url = API::ApiRequestParams.get_request_url(@params)
    url.should_not include(API::get_config(:api_key))
  end
  
  it "should return url with another hashkey, because timestamp is included into url" do 
    hash = API::ApiRequestParams.generate_hashkey(@params)
    url = API::ApiRequestParams.get_request_url(@params)
    url.should_not include(hash)
  end
end