require "rubygems"
require "rack/test"
require 'dm-rspec'
require 'webmock/rspec'

ENV['RACK_ENV'] = "test"

require File.expand_path(File.dirname(__FILE__) + "/../boot")

RSpec.configure do |config|
  config.include Rack::Test::Methods
  config.include DataMapper::Matchers
  
  #Transactions rollback for data mapper
  config.before(:each) do
    repository(:default) do
      transaction = DataMapper::Transaction.new(repository)
      transaction.begin
      repository.adapter.push_transaction(transaction)
    end
  end
  
  config.after(:each) do
    repository(:default).adapter.pop_transaction.rollback
  end
end

WebMock.disable_net_connect!(:allow_localhost => true)

def sha1 input
  Digest::SHA1.hexdigest(input)
end



