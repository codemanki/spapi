require 'spec_helper'

describe ApiRequest do
  it { should have_property :id }
  it { should have_property :hashkey  }
  it { should have_property :response }
  it { should have_property :created_at }
 
  it "should return null form cache" do
    ApiRequest.get_from_cache(:very_random_string).should be_nil
  end
  
  it "should put to cache" do
    ApiRequest.put_to_cache(:hashkey_1, "data").should_not be_nil
  end
  
  it "should put to cache value and read it" do
    key = :hashkey_2
    expected = ApiRequest.put_to_cache(key, "data")
    expected.should_not be_nil
    
    result = ApiRequest.get_from_cache(key)
    result.should_not be_nil
    result.id.should eql expected.id
  end
  
  it "should put 2 values with same key to cache but return only latest one" do
    key = :hashkey_4
    ApiRequest.put_to_cache(key, "data3")
    ApiRequest.put_to_cache(key, "data4")
    
    result = ApiRequest.get_from_cache(key)
    result.should_not be_nil
    result.response.should eql "data4"
  end
  
  it "should put value to cache but not return it due to invalidation" do 
    key = :hashkey_5
    record = ApiRequest.put_to_cache(key, "data5")
    
    record.created_at = Time.now - API.get_config(:cache_invalidation_period) - 100
    record.save
    
    ApiRequest.get_from_cache(key).should be_nil
  end
end