(function($, SP, ko){
	
	var App = function(){
		var that = this;
		
		var submitCallback = function(params){
			that.submitParams(params);
		};
		
		this.viewModel = new SP.ViewModel(submitCallback);
		this.proxy = new SP.Proxy();
		ko.applyBindings(this.viewModel, $(".contentContainer")[0]);	
	};
	
	/* Callback for submit button */
	App.prototype.submitParams = function(params){
		var that = this;
		
		//Show loader
		this.viewModel.isLoading(true);
		
		this.proxy.askApi(params, function(results){
			if (results && results["code"] && results["code"].toLowerCase() != "ok") {
				that.viewModel.initFromError(results["code"]);
			} else {
				that.viewModel.initFromResults(results, params);
			}
			that.viewModel.isLoading(false);
		}, function(){
			that.viewModel.initFromError();
			that.viewModel.isLoading(false);
		});
	};
	
	// init 
	$(function(){
		var appInstance = new App();
		$("#preLoader").hide();
	});
})(jQuery, window.SP, ko);