(function($, SP){
	// Simple proxy for server-client
	SP.Proxy = function(){
		this.url = "/askApi"
	};
	
	SP.Proxy.prototype.askApi = function(data, onSuccess, onError) {
		$.post(this.url , {params: data}).done(onSuccess).fail(onError);
	};
	
})(jQuery, window.SP, ko);