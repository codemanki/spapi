(function($, SP){
	
	var offer = function(rawOffer){
		this.title = rawOffer.title;
		this.payout = rawOffer.payout;
		
		if(rawOffer.thumbnail && rawOffer.thumbnail.lowres)
			this.thumbnail = rawOffer.thumbnail.lowres
	};
	
	//Simply attach to global scope
	SP.ViewModel = function(submitCallback){
		var that = this;
		var params = null; //here we store params for 
		
		this.offers = ko.observableArray([]); //list of offers
		this.totalPages = ko.observableArray([]); //since ko doesn't work normally with for loop, hack with array is introduced
		this.currentPage = ko.observable(0); //current page index
		this.searchPerformed = ko.observable(false); //Flag to indicate, if user submitted form. We cannot rely on offers count, since it can be null
		this.isLoading = ko.observable(false); //show loader
		
		//Search criterias. keeping both values and flag errors
		this.searchParams = {
			uid: {val: ko.observable(null), isInvalid: ko.observable(false)},
			pub0: {val: ko.observable(null), isInvalid: ko.observable(false)},
			page: {val: ko.observable(1), isInvalid: ko.observable(false)}
		};
		
		this.error = ko.observable(false);
		this.errorMessage = ko.observable("");
		/* Method, which is executed on Demo data button click */
		this.demoData = function(){
			that.searchParams.uid.val("player1");
			that.searchParams.pub0.val("campaign1");
			that.searchParams.page.val("1");
			
			clearErrors();
		};
		
		/* Method, which is executed on Clean data button click */
		this.clearData = function(){
			that.searchParams.uid.val(null);
			that.searchParams.pub0.val(null);
			that.searchParams.page.val(1);
			
			clearErrors();
		};
		
		/* Submt button click. Validates input and passes it to callback if everythin is OK*/
		this.submitSearch = function(){
			var valid = true;
			
			valid = validateSingleInput(that.searchParams.uid) && valid; // post validation, so we will show all error at once, not one by one
			valid = validateSingleInput(that.searchParams.pub0) && valid;
			valid = validateSingleInput(that.searchParams.page) && valid;
			
			if(valid) {
				clearErrors();
				clearSearchResults.call(this); //clear table
				submitCallback({uid: that.searchParams.uid.val(), pub0: that.searchParams.pub0.val(), page: that.searchParams.page.val()});
			}
		};
		
		/* Paginator click */
		this.getPage = function(page){
			submitCallback({uid: params.uid, pub0: params.pub0, page: page});
			clearSearchResults.call(that); //clear table
		};
		
		/* Should be called when api request returned error */
		this.initFromError = function(code){
			this.error(true);
			
			switch(code){
				case "ERROR_INVALID_PAGE":
					this.errorMessage("This page doesn't exist for this result");
				break;
				
				case "ERROR_INVALID_UID":
					this.errorMessage("Invalid UID");
				break;
				
				default:
					this.errorMessage("Sorry, some error occurred during search :(");
				break;
			}
		};
		
		/* Transform results from api to viewmodel format*/
		this.initFromResults = function(results, _params){
			var that = this;
			params = _params;
			this.searchPerformed(true);
			this.currentPage(params.page);
			//hack for speed :(
			for(var i = 1;  i <= results.pages; i++){
				this.totalPages.push(i);
			}
			
			$.each(results.offers, function(_, element){
				that.offers.push(new offer(element));
			});
		};
			
		/* Removes all validation results from form */
		var clearErrors = function(){
			that.searchParams.uid.isInvalid(false);
			that.searchParams.pub0.isInvalid(false);
			that.searchParams.page.isInvalid(false);
		};
		
		/* Clears model after search */
		var clearSearchResults = function(){
			params = null;
			this.offers([]);
			this.totalPages([]);
			this.currentPage(0);
			this.searchPerformed(false);
			this.error(false);
			this.errorMessage("");
		};
		
		/* Validates input, right now only string length*/
		var validateSingleInput = function(input){
			var val = input.val();
			if(val != null && val.toString().length > 0)
				return true;
			else {
				input.isInvalid(true);
				return false;
			}
		}
	};
	
})(jQuery, window.SP);